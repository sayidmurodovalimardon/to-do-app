package com.ali.todoapp.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.ali.todoapp.R
import com.ali.todoapp.R.color.sariq
import com.ali.todoapp.util.User
import kotlinx.android.synthetic.main.item_todo.view.*

class Adapter(var list:ArrayList<User>, val onClick:(user:User) ->Unit):RecyclerView.Adapter<Adapter.ViewHolder>() {
    inner class ViewHolder(itemView:View):RecyclerView.ViewHolder(itemView) {

        fun onBind(user: User) {
            itemView.apply {
                when (user.degre) {
                    1 -> {
                        color.setBackgroundColor(ContextCompat.getColor(itemView.context,R.color.telegram))
                    }
                    2 -> {
                        color.setBackgroundColor(ContextCompat.getColor(itemView.context,R.color.sariq))
                    }
                    else -> {
                        color.setBackgroundColor(ContextCompat.getColor(itemView.context,R.color.qizil))
                    }
                }

                title.text = user.title
                deadline.text = user.deadline.toString()
            }
            itemView.setOnClickListener {
                onClick(user)
            }



        }

//        override fun onClick(v: View?) {
//            val pos=adapterPosition
//            if (pos != RecyclerView.NO_POSITION){
//                listener.onClick(pos)
//            }
//        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_todo,parent,false))
    }

    override fun getItemCount(): Int =list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind(list[position])
    }
//    interface OnClick{
//        fun onClick(position: Int)
//    }
}