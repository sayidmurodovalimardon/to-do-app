package com.ali.todoapp.data

import com.ali.todoapp.util.User

interface Datas {
    fun insert(user: User)
    fun read(): ArrayList<User>
    fun readReverse(): ArrayList<User>
    fun delete(id:String?)
    fun update(user: User,id: String?)
}