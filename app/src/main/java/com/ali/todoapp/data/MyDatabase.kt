package com.ali.todoapp.data

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.ali.todoapp.util.User

class MyDatabase(context: Context):SQLiteOpenHelper(context,Contains.DATABASE_NAME,null,1),Datas {
    override fun onCreate(db: SQLiteDatabase?) {
        var query="create table ${Contains.TABLE_NAME}(${Contains.ID} integer primary key autoincrement,${Contains.NAME} text,${Contains.DATA} integer,${Contains.DEAGRE} integer)"
        db?.execSQL(query)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {

    }

    override fun insert(user: User) {
        val db=this.writableDatabase
        var cv=ContentValues()
        cv.put(Contains.NAME,user.title)
        cv.put(Contains.DATA,user.deadline)
        cv.put(Contains.DEAGRE,user.degre)
        db.insert(Contains.TABLE_NAME,null,cv)
        db.close()
    }

    override fun read(): ArrayList<User> {
        var list=ArrayList<User>()
        val db=this.readableDatabase
        var query="select * from ${Contains.TABLE_NAME}"
        var result=db.rawQuery(query,null)
        if (result.moveToFirst()){
            do {
                var user=User()
                user.id=result.getInt(result.getColumnIndex(Contains.ID))
                user.title=result.getString(result.getColumnIndex(Contains.NAME))
                user.deadline=result.getInt(result.getColumnIndex(Contains.DATA))
                user.degre=result.getInt(result.getColumnIndex(Contains.DEAGRE))
                list.add(user)

            }while (result.moveToNext())
        }
        result.close()
        db.close()

        return list

    }

    override fun readReverse(): ArrayList<User> {
        var list=ArrayList<User>()
        val db=this.readableDatabase
        var query="select * from ${Contains.TABLE_NAME} order by ${Contains.DATA}"
        var result=db.rawQuery(query,null)
        if (result.moveToFirst()){
            do {
                var user=User()
                user.id=result.getInt(result.getColumnIndex(Contains.ID))
                user.title=result.getString(result.getColumnIndex(Contains.NAME))
                user.deadline=result.getInt(result.getColumnIndex(Contains.DATA))
                user.degre=result.getInt(result.getColumnIndex(Contains.DEAGRE))
                list.add(user)

            }while (result.moveToNext())
        }
        result.close()
        db.close()

        return list
    }

    override fun delete(id: String?) {
val db=this.writableDatabase
        db.delete(Contains.TABLE_NAME,"${Contains.ID} LIKE ?", arrayOf(id))
        db.close()
    }

    override fun update(user: User, id: String?) {
val db=this.writableDatabase
        var cv=ContentValues()
        cv.put(Contains.NAME,user.title)
        cv.put(Contains.DATA,user.deadline)
        cv.put(Contains.DEAGRE,user.degre)
        db.update(Contains.TABLE_NAME,cv,"${Contains.ID} = $id", null)
        db.close()
    }

}