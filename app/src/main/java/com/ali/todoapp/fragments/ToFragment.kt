package com.ali.todoapp.fragments

import android.opengl.Visibility
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.ali.todoapp.R
import com.ali.todoapp.data.MyDatabase
import com.ali.todoapp.databinding.FragmentToBinding
import com.ali.todoapp.util.User


class ToFragment : Fragment() {

private lateinit var binding: FragmentToBinding
    private lateinit var db:MyDatabase
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        var rang=""
        // Inflate the layout for this fragment
        binding=FragmentToBinding.inflate(inflater, container, false)
        db= MyDatabase(requireContext())

        binding.save.setOnClickListener {
            if (binding.title.text.isNullOrEmpty()||binding.deadline.text.isNullOrEmpty()||rang.isNullOrEmpty())
            {
                Toast.makeText(requireContext(), "Fill all", Toast.LENGTH_SHORT).show()

            }else{
                var deg=0
                if (rang.equals("Low")){
                    deg=1
                }else
                if (rang.equals("High")){
                    deg=3
                }else
                if (rang.equals("Medium")){
                    deg=2
                }
                var user=User(binding.title.text.toString(),binding.deadline.text.toString().toInt(),deg)
                db.insert(user)
                Toast.makeText(requireContext()
                    , "Saved", Toast.LENGTH_SHORT).show()
                findNavController().popBackStack()
            }
        }
        var list= arrayListOf<String>( "Low","Medium","High")
binding.spinner.adapter=ArrayAdapter<String>(requireContext(),android.R.layout.simple_expandable_list_item_1,list)
        binding.spinner.onItemSelectedListener=object :AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
rang="Low"
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
rang=list.get(position)
            }

        }
        return binding.root
    }


}