package com.ali.todoapp.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import com.ali.todoapp.R
import com.ali.todoapp.data.MyDatabase
import com.ali.todoapp.databinding.FragmentUpdateBinding
import com.ali.todoapp.util.User


class UpdateFragment : Fragment() {

private lateinit var binding: FragmentUpdateBinding
    private lateinit var db: MyDatabase
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding=FragmentUpdateBinding.inflate(inflater, container, false)
        db= MyDatabase(requireContext())
        var rang=""
        var user=arguments?.getSerializable("kod") as User
        if (user.title.isNullOrEmpty())
        {
            binding.update.visibility=View.GONE
            binding.delete.visibility=View.GONE
        }else{
            binding.title.setText(user.title)
            binding.deadline.setText(user.deadline.toString())
            binding.update.visibility=View.VISIBLE
            binding.delete.visibility=View.VISIBLE

            binding.delete.setOnClickListener {
                db.delete(user.id.toString())
                Toast.makeText(requireContext(), "deleted", Toast.LENGTH_SHORT).show()
                findNavController().popBackStack()
            }

            binding.update.setOnClickListener {
                if (binding.title.text.isNullOrEmpty()||binding.deadline.text.isNullOrEmpty()||rang.isNullOrEmpty()){
                    Toast.makeText(requireContext(), "Fill all gaps", Toast.LENGTH_SHORT).show()
                }else{
                    var deg=0
                    if (rang.equals("Low")){
                        deg=1
                    }else
                        if (rang.equals("High")){
                            deg=3
                        }else
                            if (rang.equals("Medium")){
                                deg=2
                            }
                    var user1= User(binding.title.text.toString(),binding.deadline.text.toString().toInt(),deg)

                    db.update(user1,user.id.toString())
                    Toast.makeText(requireContext(), "updated", Toast.LENGTH_SHORT).show()
                    findNavController().popBackStack()
                }

            }
        }

        binding.back.setOnClickListener {
            findNavController().popBackStack()
        }
        var list= arrayListOf<String>( "Low","Medium","High")
        binding.spinner.adapter= ArrayAdapter<String>(requireContext(),android.R.layout.simple_expandable_list_item_1,list)
        binding.spinner.onItemSelectedListener=object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                rang="Low"
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                rang=list.get(position)
            }

        }
        return binding.root
    }


}