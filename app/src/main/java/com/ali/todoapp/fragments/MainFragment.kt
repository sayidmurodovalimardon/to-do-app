package com.ali.todoapp.fragments

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.view.GravityCompat
import androidx.navigation.fragment.findNavController
import com.ali.todoapp.R
import com.ali.todoapp.adapter.Adapter
import com.ali.todoapp.data.MyDatabase
import com.ali.todoapp.databinding.FragmentMainBinding
import com.ali.todoapp.shared.MyShared
import com.ali.todoapp.util.User
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.navigation_layout.view.*


class MainFragment : Fragment(), NavigationView.OnNavigationItemSelectedListener {

    private lateinit var binding: FragmentMainBinding
    private lateinit var db:MyDatabase
    private lateinit var list:ArrayList<User>
    private lateinit var adapter:Adapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMainBinding.inflate(inflater, container, false)
        db= MyDatabase(requireContext())



        setAdapter(db.read())


        binding.navigation.bringToFront()
        binding.navigation.setNavigationItemSelectedListener(this)
        binding.menu.setOnClickListener {
            if (binding.draver.isDrawerVisible(GravityCompat.START)) {
                binding.draver.closeDrawer(GravityCompat.START)
            } else {
                binding.draver.openDrawer(GravityCompat.START)
            }
        }

        binding.add.setOnClickListener {

            findNavController().navigate(R.id.toFragment)
        }
        return binding.root
    }

    private fun setAdapter(list: ArrayList<User>) {
        if (list.isNullOrEmpty()) {
        }else{
            adapter=Adapter(list){
                var fragment=UpdateFragment()
                var bundle=Bundle()
                bundle.putSerializable("kod",it)
                fragment.arguments=bundle
                findNavController().navigate(R.id.updateFragment,bundle)
            }
            binding.recyler.adapter=adapter

        }
    }


    override fun onNavigationItemSelected(item: MenuItem): Boolean {


        when(item.itemId){
            R.id.conection ->{Toast.makeText(requireContext(), "Created by @A_Sayidmurodov", Toast.LENGTH_SHORT)
                .show()}
            R.id.reverse ->{setAdapter(db.readReverse())}
        }

        return true
    }




}


