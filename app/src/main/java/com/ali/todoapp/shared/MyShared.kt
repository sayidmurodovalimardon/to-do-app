package com.ali.todoapp.shared

import android.content.Context
import android.content.SharedPreferences
import java.lang.reflect.Modifier

object MyShared {
    private const val NAME="my_android"
    private const val MODE=Context.MODE_PRIVATE
    private lateinit var prefernce:SharedPreferences

    fun init(context: Context){
        prefernce=context.getSharedPreferences(NAME, MODE)
    }
    private inline fun SharedPreferences.edit(operation: (SharedPreferences.Editor) -> Unit) {
        val editor: SharedPreferences.Editor = edit()
        operation(editor)
        editor.apply()
    }

    var checked: Boolean
        get() = prefernce.getBoolean("user",false)
        set(value) = prefernce.edit(){
            it.putBoolean("user",value)
        }
}






