package com.ali.todoapp.util

import java.io.Serializable

class User :Serializable {
    var title: String = ""
    var deadline: Int = 0
    var degre:Int=0
    var id:Int=0

    constructor(title: String, deadline: Int, degre: Int) {
        this.title = title
        this.deadline = deadline
        this.degre = degre
    }

    constructor(){

    }

    constructor(title: String, deadline: Int, degre: Int, id: Int) {
        this.title = title
        this.deadline = deadline
        this.degre = degre
        this.id = id
    }

}